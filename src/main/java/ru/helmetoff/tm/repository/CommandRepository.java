package ru.helmetoff.tm.repository;

import ru.helmetoff.tm.api.repository.ICommandRepository;
import ru.helmetoff.tm.constant.ArgumentConst;
import ru.helmetoff.tm.constant.TerminalConst;
import ru.helmetoff.tm.model.Command;

import java.util.Arrays;

public class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP, "Display list of terminal commands."
    );

    private static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT, "Display developer info."
    );

    private static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION, "Display program version."
    );

    private static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO, "Display information about system."
    );

    private static final Command SHOW_COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Display list of commands."
    );

    private static final Command SHOW_ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Display list of arguments."
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task."
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Delete all tasks."
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Display tasks list."
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project."
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Delete all projects."
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Display projects list."
    );

    private static final Command EXIT = new Command(
            TerminalConst.EXIT, null, "Close Application."
    );

    private static Command[] TERMINAL_COMMANDS = new Command[]{
            HELP, ABOUT, VERSION, INFO, SHOW_COMMANDS, SHOW_ARGUMENTS,
            TASK_CREATE, TASK_LIST, TASK_CLEAR,
            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR,
            EXIT
    };

    private String[] COMMANDS = getCommands(TERMINAL_COMMANDS);

    private String[] ARGUMENTS = getArguments(TERMINAL_COMMANDS);

    public String[] getCommands(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String name = values[i].getName();
            if (name == null || name.isEmpty()) continue;
            result[index] = name;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public String[] getArguments(Command... values) {
        if (values == null || values.length == 0) return new String[]{};
        final String[] result = new String[values.length];
        int index = 0;
        for (int i = 0; i < values.length; i++) {
            final String arg = values[i].getArg();
            if (arg == null || arg.isEmpty()) continue;
            result[index] = arg;
            index++;
        }
        return Arrays.copyOfRange(result, 0, index);
    }

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

    public String[] getCommands() {
        return COMMANDS;
    }

    public String[] getArguments() {
        return ARGUMENTS;
    }

}
